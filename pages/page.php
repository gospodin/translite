<?php
class HTML
{
    function showHash()
    {
        $hash = substr(trim(file_get_contents("../.git/refs/heads/master")), 0, 7);
        $url = "https://codeberg.org/gospodin/translite/commit/$hash";
        echo "<a href=\"$url\" target=\"_blank\">Latest commit: $hash</a>";
    }
    function renderHTML()
    {
        include "elements/header.php";
        include get_class($this).".view.php";
        include "elements/footer.php";
    }

}
