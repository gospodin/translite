<?php
require_once "parser.php";

class ddgParser extends Parser
{
    public $parserName = "ddg";
    private $config;
    function __construct($config)
    {
        $this->config = $config;
    }
    function getLanguages($mui)
    {
        $cachedLangs = apcu_fetch($mui."_langs_ddg");
        if ($cachedLangs !== false) {
            return $cachedLangs;
        }
        $result = [];
        if ($mui == "sl") {
            $result["auto"] = "Autodetect";
        }

        /* I'm sorry */
        $response = $this->requestGet("https://duckduckgo.com/dist/util/u.a94849abb631fa45ce7f.js");
        $match = [];
        preg_match('/translations:\[((\{id:"[a-zA-Z-]*",name:lp\("language_name","[^"]*"\),?[^}]*\},?)*)\]/', $response, $match);
        $langs = preg_split('/(regions:\[[^]]*\])?},?{?/', $match[1]);
        foreach($langs as $lang) {
            $match2 = [];
            preg_match('/id:"([a-zA-Z-]*)",name:lp\("language_name","([^"]*)"\)/', $lang, $match2);
            if (!isset($match2[1]) || !isset($match2[2]) || $match2[1] == "" || $match2[2] == "") {
                continue;
            }
            $result[$match2[1]] = $match2[2];
        }

        apcu_store($mui."_langs_ddg", $result, $this->config->ttl);
        return $result;
    }
    private function _getTranslation($text, $sl, $tl) {
        $cachedTranslation = apcu_fetch($sl."_".$tl."_".$text."_ddg");
        if ($cachedTranslation !== false) {
            return json_decode($cachedTranslation);
        }
        $match = [];
        preg_match('/vqd="([^"]*)"/', $this->requestGet("https://duckduckgo.com/?q=translate"), $match);
        $token = $match[1];
        unset($match);
        $url = "https://duckduckgo.com/translation.js?vqd=$token&query=translate&to=$tl" . ($sl == "auto" ? "" : "&from=$sl");
        $response = $this->requestPost($url, $text);
        apcu_store($sl."_".$tl."_".$text."_ddg", $response, $this->config->ttl);
        return json_decode($response);
    }
    function translate($text, $sl, $tl)
    {
        return $this->_getTranslation($text, $sl, $tl)->translated;
    }
    function getAdditionalData($text, $sl, $tl)
    {
        $translationData = new TranslationData();
        $translationData->sourceLang = $this->_getTranslation($text, $sl, $tl)->detected_language;
        return $translationData;
    }
    function TranslateButton() {
        return "Translate";
    }
}