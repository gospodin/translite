<?php
require_once "parser.php";

class deeplParser extends Parser
{
    public $parserName = "deepl";
    private $config;
    private $apiURL = "https://www2.deepl.com/jsonrpc";
    private $id;
    function __construct($config)
    {
        $this->config = $config;
        $this->id = (rand(0, 99999) + 8300000) * 1000;
    }
    function _translationGet($text, $sl, $tl)
    {
        $cachedTranslation = apcu_fetch($sl."_".$tl."_".$text."_deepl_result");
        if ($cachedTranslation !== false) {
            return $cachedTranslation;
        }
        $this->checkLanguages($sl, $tl);
        $this->id++;
        $timestamp = floor(microtime(true) * 1000);
        $iCount = substr_count($text, "i");
        if ($iCount != 0) {
            $iCount += 1;
            $fakeTimestamp = $timestamp - ($timestamp % $iCount) + $iCount;
        } else {
            $fakeTimestamp = $timestamp;
        }
        $parameter = [
            "jsonrpc" => "2.0",
            "method" => "LMT_handle_texts",
            "id" => $this->id,
            "params" => [
                "texts" => [[
                    "text" => $text,
                    "requestAlternatives" => 1
                ]],
                "splitting" => "newlines",
                "lang" => [
                    "source_lang_user_selected" => $sl,
                    "target_lang" => $tl
                ],
                "timestamp" => $fakeTimestamp,
                "commonJobParams" => [
                    "wasSpoken" => false,
                    "transcribe_as" => ""
                ]
            ],
        ];
        $encodedParameter = json_encode($parameter);
        if (($this->id + 3) % 13 == 0 || ($this->id + 5) % 29 == 0) {
            $replacement = '"method" : "';
        } else {
            $replacement = '"method": "';
        }
        $finalParameter = str_replace('"method":"', $replacement, $encodedParameter);
        $headers = [
            "Accept: */*",
            "Accept-Language: en-US,en;q=0.9,zh-CN;q=0.8,zh-TW;q=0.7,zh-HK;q=0.6,zh;q=0.5",
            "Authorization: None",
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "DNT: 1",
            "Origin: chrome-extension://cofdbpoegempjloogbagkncekinflcnj",
            "Pragma: no-cache",
            "Priority: u=1, i",
            "Referer: https://www.deepl.com/",
            "Sec-Fetch-Dest: empty",
            "Sec-Fetch-Mode: cors",
            "Sec-Fetch-Site: none",
            "Sec-GPC: 1",
            "User-Agent: DeepLBrowserExtension/1.28.0 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36"
        ];
        $response = $this->requestPost($this->apiURL, $finalParameter, $headers);
        apcu_store($sl."_".$tl."_".$text."_deepl_result", $response, $this->config->ttl);
        return $response;
    }
    function translate($text, $sl, $tl)
    {
        $textAccumulator = "";
        // split text to avoid ratelimiting
        foreach(preg_split("/.{0,1200}\K(?:\s+|$)/s", $text, 0, PREG_SPLIT_NO_EMPTY) as $i) {
            $result = $this->_translationGet($i, $sl, $tl);
            $texts = json_decode($result)->result->texts;
            foreach ($texts as $line) {
                $textAccumulator .= $line->text;
                $textAccumulator .= "\n";
            }
            sleep(3);
        }
        return $textAccumulator;
    }
    function getLanguages($mui)
    {
        $langs = [
            "bg" => "Bulgarian",
            "zh" => "Chinese",
            "cs" => "Czech",
            "da" => "Danish",
            "nl" => "Dutch",
            "en" => "English",
            "et" => "Estonian",
            "fi" => "Finnish",
            "fr" => "French",
            "de" => "German",
            "el" => "Greek",
            "hu" => "Hungarian",
            "it" => "Italion",
            "ja" => "Japanese",
            "lv" => "Latvian",
            "lt" => "Lithuanian",
            "pl" => "Polish",
            "pt" => "Portugese",
            "ro" => "Romanian",
            "ru" => "Russian",
            "sk" => "Slovak",
            "sl" => "Slovenian",
            "es" => "Spanish",
            "sv" => "Swedish",
        ];
        if ($mui == "sl") {
            $langs["auto"] = "Autodetect";
        }
        return $langs;
    }
    function TranslateButton()
    {
        return "Translate";
    }
    private function _getDictionary($text, $sl, $tl, &$trData)
    {
        if (strlen($text) > 100) {
            return null;
        }
        $languagesWithNames = [
            "bg" => "bulgarian",
            "cs" => "czech",
            "da" => "danish",
            "de" => "german",
            "el" => "greek",
            "en" => "english",
            "es" => "spanish",
            "et" => "estonian",
            "fi" => "finnish",
            "fr" => "french",
            "hu" => "hungarian",
            "it" => "italian",
            "ja" => "japanese",
            "lt" => "lithuanian",
            "lv" => "latvian",
            "mt" => "maltese",
            "nl" => "dutch",
            "pl" => "polish",
            "pt" => "portuguese",
            "ro" => "romanian",
            "ru" => "russian",
            "sk" => "slovak",
            "sl" => "slovene",
            "sv" => "swedish",
            "zh" => "chinese",
        ];
        if (!array_key_exists($sl, $languagesWithNames) or !array_key_exists($tl, $languagesWithNames)) {
            return null;
        }
        $url = "https://dict.deepl.com/".$languagesWithNames[$sl]."-".$languagesWithNames[$tl]."/search?ajax=1&source=".$languagesWithNames[$sl]."&onlyDictEntries=1&translator=dnsof7h3k2lgh3gda&delay=800&jsStatus=0&kind=full&eventkind=keyup&forleftside=true";
        $xpath = $this->getXpath($this->requestPost($url, "query=$text"));
        foreach ($xpath->query('//*[@id="dictionary"]/div/div[1]/div') as $lemma) {
            $type = $xpath->query("div/h2/span[1]/span[1]", $lemma)->item(0)->nodeValue;
            $translations = [];
            foreach ($xpath->query(".//a[contains(@id,'dictEntry')]", $lemma) as $translation) {
                $examples = [];
                foreach (@$xpath->query("./../../../div[@class='example_lines']/div/span", $translation) as $example) {
                    $sourceExample = $xpath->query("./span[@class='tag_s']", $example)->item(0)->nodeValue;
                    $targetExample = $xpath->query("./span[@class='tag_t']", $example)->item(0)->nodeValue;
                    $examples[] = "$sourceExample --<br> $targetExample";
                }
                $trData->addDictionaryWord($type, $translation->nodeValue, "Examples", ...$examples);
            }
        }

    }
    function getAdditionalData($text, $sl, $tl)
    {
        $translationData = new TranslationData();
        $this->_getDictionary($text, $sl, $tl, $translationData);
        $translationData->sourceLang = strtolower(json_decode($this->_translationGet($text, $sl, $tl))->result->lang);
        $translationData->slPronunciation = "";
        $translationData->tlPronunciation = "";
        return $translationData;
    }
}
