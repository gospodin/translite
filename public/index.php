<?php
session_start();
$config = include "../config/config.php";
include_once $config->parsers[$config->parser]->file;
$parserClassName = $config->parser . "Parser";
$parser = new $parserClassName($config);

$request = rtrim(strtok($_SERVER['REQUEST_URI'], "?"), "/");
if (str_contains($request, "/api")) {
    require_once "../pages/api.php";
}
if (str_contains($request, "/api/toengine/")) {
    $engine = str_replace("/api/toengine/", '', $request);
    $request = str_replace("/$engine", '', $request);
}
if (str_contains($request, "/settings")) {
    require_once "../pages/settings.php";
}

// get rid of the class creation on each request, TODO
switch ($request) {
case '':
    require_once "../pages/translate.php";
    (new translate($parser, $config))->renderHTML();
    break;
case '/settings':
    (new settings($config))->renderHTML();
    break;
case '/settings/set':
    (new settings($config))->set();
    break;
case '/settings/reset':
    (new settings($config))->reset();
    break;
case '/api':
    (new API($parser, $config))->getHelpText();
    break;
case '/api/translate':
    (new API($parser, $config))->translate();
    break;
case '/api/get_languages':
    (new API($parser, $config))->getLanguages();
    break;
case '/api/tts':
    (new API($parser, $config))->TTS();
    break;
case '/api/additional_data':
    (new API($parser, $config))->getAdditionalData();
    break;
case '/api/swap':
    (new API($parser, $config))->swapLanguages();
    break;
case '/api/toengine':
    (new API($parser, $config))->toEngine($engine);
    break;
case '/api/clear_text':
    (new API($parser, $config))->clearText();
    break;
default:
    http_response_code(404);
}
?>
