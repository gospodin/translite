<form class="center" method="post" action="settings/set">
    <div>
        <label for="parser">Select your preferred engine: </label>
        <select name="parser" id="parser">
            <?php $this->printParsers() ?>
        </select>
    </div>
    <div>
        <label for="selector">Select your language selector: </label>
        <select name="selector" id="selector">
            <?php $this->printLangSelectors() ?>
        </select>
    </div>
    <div>
        <?php $this->printDefaultLanguageSelectors() ?>
    </div>
    <div>
        <?php $this->printJSToggle() ?>
    </div>
    <div>
        <?php $this->printPOSTToggle() ?>
    </div>
    <div>
        <button type="submit">Save</button>
        <button type="submit" formaction="settings/reset">Reset</button>
    </div>
</form>
