<!DOCTYPE html>
<html lang="en">
    <head>
        <title>TransLite</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="/static/styles.css">
        <?php if ($this->config->jsEnabled) {?>
        <link rel="stylesheet" href="/static/autogrow.css">
        <?php } ?>
        <link rel="icon" type="image/png" href="/static/favicon.png">
    </head>
    <body>
        <header class="center">
            <h1><a href="/">TransLite</a></h1>
        </header>
