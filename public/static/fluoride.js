const form = document.getElementById("form")
form.addEventListener("submit", (event) => {
	if (event.submitter.id == "swap") {
		event.preventDefault();
		var sl = document.getElementById("slSelect");
		var tl = document.getElementById("tlSelect");
		if (sl.value == "auto"){
			alert("Can't translate to autodetect");
			return;
		}
		var temp = sl.value;
		sl.value = tl.value;
		tl.value = temp;
		document.getElementById("slTextarea").focus();
	}
	if (event.submitter.id == "clear") {
		event.preventDefault();
		document.getElementById("slTextarea").value = "";
		document.getElementById("slTextarea").focus();
	}
})

let textAreas = document.querySelectorAll("textarea");
textAreas.forEach((textArea) => {
	textArea.style.height = "auto";
	textArea.style.height = textArea.scrollHeight - 4 + "px";
});

let textAreaSl = textAreas.item(0);
textAreaSl.addEventListener("input", () => {
	textAreaSl.style.height = "auto";
	textAreaSl.style.height = textAreaSl.scrollHeight - 4 + "px";
});
