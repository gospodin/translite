<?php
$config = (object) array(
    "hl" => "en",
    "ttl" => 604800,
    "parser" => "google",
    "maxLinkLength" => 3700, //adjust for server configuration
    "parsers" => [
        "google" => (object) [
            "name" => "Google",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/google.php"
        ],
        "deepl" => (object) [
            "name" => "DeepL",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/deepl.php"
        ],
        "yandex" => (object) [
            "name" => "Yandex",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/yandex.php"
        ],
        "googleD" => (object) [
            "name" => "GoogleD",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/google-d.php"
        ],
        "ddg" => (object) [
            "name" => "DuckDuckGo",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/ddg.php"
        ],
        "multi" => (object) [
            "name" => "Multiple engines",
            "prefSl" => "auto",
            "prefTl" => "en",
            "file" => "../parsers/multi.php"
        ],
    ],
    "prefLangSelect" => "select", //can be datalist or input
    "keybinds" => (object) [
        "sl" => ",",
        "tl" => ".",
        "slTTS" => "h",
        "slText" => "j",
        "tlTTS" => "l",
        "tlText" => "k",
        "swap" => "s",
        "translate" => "t",
        "clearText" => "c"
    ],
    "usePOST" => false,
    "jsEnabled" => false,
    "selected_parsers_array" => null,
);

if (isset($_COOKIE['parser'])) {
    $config->parser = basename($_COOKIE['parser']);
}
if (isset($_GET['engine'])) {
    $config->parser = basename($_GET['engine']);
} else if (isset($_POST['engine'])) {
    $config->parser = basename($_POST['engine']);
} else if (isset($_SESSION['request']['engine'])) {
    $config->parser = basename($_SESSION['request']['engine']);
    unset($_SESSION['request']['engine']);
}
if (isset($_GET['engines'])) {
    $config->selected_parsers_array = $_GET['engines'];
} else if (isset($_POST['engines'])) {
    $config->selected_parsers_array = $_POST['engines'];
} else if (isset($_SESSION['request']['engines'])) {
    $config->selected_parsers_array = $_SESSION['request']['engines'];
    unset($_SESSION['request']['engines']);
}
if (isset($_COOKIE['selector'])) {
    $config->prefLangSelect = $_COOKIE['selector'];
}
if (isset($_COOKIE['js']) && $_COOKIE['js'] == "on") {
    $config->jsEnabled = true;
}
if (isset($_COOKIE['post']) && $_COOKIE['post'] == "on") {
    $config->usePOST = true;
}
foreach ($config->parsers as $parserValue => $parserData) {
    if (isset($_COOKIE[$parserValue])) {
        $config->parsers[$parserValue]->prefSl = $_COOKIE[$parserValue]['prefSl'];
        $config->parsers[$parserValue]->prefTl = $_COOKIE[$parserValue]['prefTl'];
    }
}
return $config;
