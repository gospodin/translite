<?php
require_once "page.php";
class settings extends HTML
{
    protected $config;
    protected $selectors = ["select" => "Select", "datalist" => "Datalist", "input" => "Regular input"];
    function __construct($config)
    {
        $this->config = $config;
    }
    function printParsers()
    {
        foreach ($this->config->parsers as $parserValue => $parserData) {
            $selected = "";
            if ($parserValue == $this->config->parser) {
                $selected = "selected";
            }
            echo "<option $selected value=$parserValue>$parserData->name</option>";
            unset($selected);
        }
    }
    function printLangSelectors()
    {
        foreach ($this->selectors as $selectorValue => $selectorName) {
            $selected = "";
            if ($selectorValue == $this->config->prefLangSelect) {
                $selected = "selected";
            }
            echo "<option $selected value=$selectorValue>$selectorName</option>";
            unset($selected);
        }
    }
    function printDefaultLanguageSelectors()
    {
        foreach ($this->config->parsers as $parserValue => $parserData) {
            if ($parserValue == "multi") {
                continue;
            }
            include_once $parserData->file;
            $parserClassName = $parserValue . "Parser";
            $parser = new $parserClassName($this->config);
            echo "<fieldset>";
            echo "<legend>Choose your default languages for $parserData->name</legend>";
            echo "<select name=\"{$parserValue}[prefSl]\">";
            foreach ($parser->getLanguages('sl') as $value => $text) {
                $selected = $value == $this->config->parsers[$parserValue]->prefSl;
                if ($selected) {
                    echo "<option selected value=\"$value\">$text</option>";
                } else {
                    echo "<option value=\"$value\">$text</option>";
                }
            }
            echo "</select>";
            echo "<select name=\"{$parserValue}[prefTl]\">";
            foreach ($parser->getLanguages('tl') as $value => $text) {
                $selected = $value == $this->config->parsers[$parserValue]->prefTl;
                if ($selected) {
                    echo "<option selected value=\"$value\">$text</option>";
                } else {
                    echo "<option value=\"$value\">$text</option>";
                }
            }
            echo "</select>";
            echo "</fieldset>";
        }
    }
    function printJSToggle()
    {
        if ($this->config->jsEnabled) {
            $checked = "checked";
        } else {
            $checked = "";
        }
        echo "<label for=\"jsEnabled\">Enable JavaScript?</label>";
        echo "<input type=\"checkbox\" id=\"jsEnabled\" $checked name=\"js\" />";
    }
    function printPOSTToggle()
    {
        if ($this->config->usePOST) {
            $checked = "checked";
        } else {
            $checked = "";
        }
        echo "<label for=\"usePOST\">Use POST for translation?</label>";
        echo "<input type=\"checkbox\" id=\"usePOST\" $checked name=\"post\" />";
    }
    private function setSetting($name, $value) {
        setcookie($name, $value, time() + (86400 * 90), path: "/");
    }
    private function resetSetting($name) {
        setcookie($name, "", 1, path: "/");
    }
    function set()
    {
        if (array_key_exists($_POST['parser'], $this->config->parsers)) {
            $this->setSetting("parser", $_POST['parser']);
        }
        if (array_key_exists($_POST['selector'], $this->selectors)) {
            $this->setSetting("selector", $_POST['selector']);
        }
        foreach ($this->config->parsers as $parserValue => $parserData) {
            $this->setSetting("{$parserValue}[prefTl]", $_POST[$parserValue]['prefTl']);
            $this->setSetting("{$parserValue}[prefSl]", $_POST[$parserValue]['prefSl']);
        }
        if (isset($_POST['js']) && $_POST['js'] == "on") {
            $this->setSetting("js", "on");
        } else {
            $this->setSetting("js", "off");
        }
        if (isset($_POST['post']) && $_POST['post'] == "on") {
            $this->setSetting("post", "on");
        } else {
            $this->setSetting("post", "off");
        }
        header("Location: /");
        die();
    }
    function reset()
    {
        $this->resetSetting("parser");
        $this->resetSetting("selector");
        foreach ($this->config->parsers as $parserValue => $parserData) {
            $this->resetSetting("{$parserValue}[prefSl]");
            $this->resetSetting("{$parserValue}[prefTl]");
        }
        $this->resetSetting("js");
        $this->resetSetting("post");
        header("Location: /");
        die();
    }
}
