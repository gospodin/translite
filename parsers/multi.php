<?php
require_once "parser.php";

class multiParser extends Parser
{
    public $parserName = "multi";
    private $config;
    function __construct($config)
    {
        $this->config = $config;
    }
    function getLanguages($mui)
    {
        $result = [];
        foreach ($this->config->selected_parsers_array as $parser) {
            if ($parser == "multi") {
                continue;
            };
            include_once $this->config->parsers[$parser]->file;
            $parserClassName = $parser . "Parser";
            $result[] = (new $parserClassName($this->config))->getLanguages($mui);
        }
        return array_intersect(...$result);
    }
    function translate($text, $sl, $tl)
    {
        $result = [];
        foreach ($this->config->selected_parsers_array as $parser) {
            if ($parser == "multi") {
                continue;
            };
            include_once $this->config->parsers[$parser]->file;
            $parserClassName = $parser . "Parser";
            $result[] = "{$this->config->parsers[$parser]->name}:\n". (new $parserClassName($this->config))->translate($text, $sl, $tl);
        }
        return implode("</textarea><textarea accesskey=\"{$this->config->keybinds->tlText}\" placeholder=\"Translation\" readonly>", $result);
    }
    function getAdditionalData($text, $sl, $tl) {
        $translationData = new TranslationData();
        foreach ($this->config->selected_parsers_array as $parser) {
            if ($parser == "multi") {
                continue;
            };
            include_once $this->config->parsers[$parser]->file;
            $parserClassName = $parser . "Parser";
            $parserName = $this->config->parsers[$parser]->name;
            $result = (new $parserClassName($this->config))->getAdditionalData($text, $sl, $tl);
            foreach($result->getDefinitions() ?? [] as $pos) {
                foreach($pos->definitions as $definition) {
                    $translationData->addDefinition($pos->pos." ($parserName)", $definition->label, $definition->definition, $definition->example);
                }
            }
            foreach($result->getExamples() ?? [] as $example) {
                $translationData->addExample("$example ($parserName)");
            }
            foreach($result->getSynonyms() ?? [] as $pos) {
                foreach($pos->synonyms as $synonym) {
                    $translationData->addSynonym($pos->pos." ($parserName)", $synonym->label, ...$synonym->synonym);
                }
            }
            foreach($result->getDictionary() ?? [] as $pos) {
                foreach($pos->words as $word) {
                    $translationData->addDictionaryWord($pos->pos." ($parserName)", $word->word, $word?->helpers?->type ?? "Examples", ...$word?->helpers?->list ?? []);
                }
            }
            /* only google has this at time of writing this comment */
            if ($result->slPronunciation !== null && $result->slPronunciation !== "") {
                $translationData->slPronunciation = $result->slPronunciation;
            }
            if ($result->tlPronunciation !== null && $result->tlPronunciation !== "") {
                $translationData->tlPronunciation = $result->tlPronunciation;
            }
        }
        return $translationData;
    }
    function translateButton() {
        return "Translate";
    }
}
