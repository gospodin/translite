<?php
class API
{
    protected $parser;
    protected $request;
    protected $config;
    function __construct(&$parser, $config)
    {
        $this->parser =& $parser;
        $this->config = $config;
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $this->request = $_POST;
        } else {
            $this->request = $_GET;
        }
    }
    function translate()
    {
        echo $this->parser->translate($this->request['text'], $this->request['sl'], $this->request['tl']);
    }
    function getLanguages()
    {
        header('Content-Type: application/json');
        $langs = array();
        $langs['sl'] = $this->parser->getLanguages('sl');
        $langs['tl'] = $this->parser->getLanguages('tl');
        echo json_encode($langs);
    }
    function TTS()
    {
        if (!method_exists($this->parser, 'tts')) {
            return null;
        }
        $this->parser->tts($this->request['text'], $this->request['tl']);
    }
    function getAdditionalData()
    {
        header('Content-Type: application/json');
        echo json_encode($this->parser->getAdditionalData($this->request['text'], $this->request['sl'], $this->request['tl']));
    }

    function swapLanguages()
    {
        $query = $this->request;
        unset($query['q']);
        $tlInSl = array_key_exists($this->request['sl'], $this->parser->getLanguages('tl'));
        $slInTl = array_key_exists($this->request['tl'], $this->parser->getLanguages('sl'));
        if (!$tlInSl || !$slInTl) {
            if (!isset($_SESSION['error'])) {
                $_SESSION['error'] = "Can't switch these languages";
            }
        }
        else {
            $query['sl'] = $this->request['tl'];
            $query['tl'] = $this->request['sl'];
        }
        if (!$this->config->usePOST) {
            header("Location: /?".http_build_query($query));
        } else {
            $_SESSION['request'] = $query;
            header ("Location: /");
        }
        die();
    }

    function toEngine($engine)
    {
        $query = $this->request;
        unset($query['q']);
        $query['engine'] = $engine;
        if ($query['sl'] == 'auto') {
            unset($query['sl']);
        }
        if (!$this->config->usePOST) {
            header("Location: /?".http_build_query($query));
        } else {
            $_SESSION['request'] = $query;
            header ("Location: /");
        }
        die();
    }

    function clearText()
    {
        $query = $this->request;
        unset($query['text']);
        unset($query['q']);
        if (!$this->config->usePOST) {
            header("Location: /?".http_build_query($query));
        } else {
            $_SESSION['request'] = $query;
            header ("Location: /");
        }
        die();
    }

    function getHelpText()
    {
        ?>
        there are 5 api endpoints:<br>
        /api/translate?sl=SOURCE_LANG&tl=TARGET_LANG&text=SOURCE_TEXT<br>
        /api/additional_data?sl=SOURCE_LANG&tl=TARGET_LANG&text=SOURCE_TEXT<br>
        /api/tts?tl=TARGET_LANG&text=SOURCE_TEXT<br>
        /api/get_languages<br>
        /api/swap<br>
        <?php
    }
}
