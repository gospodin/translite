ARG ALPINE_VERSION=3.21

FROM alpine:${ALPINE_VERSION}

LABEL org.opencontainers.image.authors="vlnst <vlnst@bloat.cat>"
LABEL org.opencontainers.image.description="Lightweight TransLite container with Nginx 1.26 & PHP 8.3 based on Alpine Linux."
LABEL org.opencontainers.image.licenses="The Unlicense"
LABEL org.opencontainers.image.source=https://git.bloat.cat/gospodin/translite
LABEL org.opencontainers.image.title=TransLite

# Setup document root
WORKDIR /var/www/html

RUN apk add --no-cache \
  curl \
  nginx \
  php83 \
  php83-brotli \
  php83-curl \
  php83-dom \
  php83-fpm \
  php83-pecl-apcu \
  php83-session \
  supervisor

# Configure nginx - http
COPY config/nginx.conf /etc/nginx/nginx.conf
# Configure nginx - default server
COPY config/conf.d /etc/nginx/conf.d/

# Configure PHP-FPM
ENV PHP_INI_DIR=/etc/php83
COPY config/fpm-pool.conf ${PHP_INI_DIR}/php-fpm.d/www.conf
COPY config/php.ini ${PHP_INI_DIR}/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody:nobody /var/www/html /run /var/lib/nginx /var/log/nginx

# Switch to use a non-root user from here on
USER nobody

# Add application
COPY --chown=nobody . /var/www/html/

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping || exit 1
