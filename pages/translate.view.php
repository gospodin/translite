        <form id="form" class="center" method="<?= $this->getMethod() ?>">
            <div class="engines">
                <?php $this->printEngines() ?>
            </div>
            <div class="langs">
                <?php $this->printLanguages('sl') ?>
                <button accesskey="<?=$this->config->keybinds->swap?>" type="submit" formaction="api/swap" id=swap>⇄</button>
                <?php $this->printLanguages('tl') ?>
            </div>
            <div class="textareas">
                <div class="item-wrapper sl-textarea">
                    <!-- prevent from accidental form clearing -->
                    <button type="submit" disabled style="display: none" aria-hidden="true"></button>
                    <button accesskey="<?=$this->config->keybinds->clearText?>" type="submit" formaction="api/clear_text" id="clear">✖</button>
                    <textarea <?php $this->printMaxLength() ?> accesskey="<?=$this->config->keybinds->slText?>" name="text" placeholder="Write text here" autofocus id="slTextarea"><?php $this->slTextArea() ?></textarea>
                    <?php $this->printPronunciation('sl')?>
                    <?php $this->slTTSLink() ?>
                </div>
                <div class=item-wrapper>
                    <textarea accesskey="<?=$this->config->keybinds->tlText?>" placeholder="Translation" readonly><?php $this->tlTextarea() ?></textarea>
                    <?php $this->printPronunciation('tl')?>
                    <?php $this->tlTTSLink() ?>
                </div>
            </div>
            <button accesskey="<?=$this->config->keybinds->translate?>" type="submit"><?php $this->translateButton() ?></button>
        </form>
        <?php $this->showError() ?>
        <div id="definitions_and_translations">
            <?php $this->printAdditionalData(); ?>
        </div>
        <details id="keybinds" class="center">
            <summary>Keybinds</summary>
            <ul>
                <li>Accesskey + <?=$this->config->keybinds->sl?> - select source language list</li>
                <li>Accesskey + <?=$this->config->keybinds->tl?> - select target language list</li>
                <li>Accesskey + <?=$this->config->keybinds->slTTS?> - select left audio</li>
                <li>Accesskey + <?=$this->config->keybinds->slText?> - select left textarea</li>
                <li>Accesskey + <?=$this->config->keybinds->tlText?> - select right textarea</li>
                <li>Accesskey + <?=$this->config->keybinds->tlTTS?> - select right audio</li>
                <li>Accesskey + <?=$this->config->keybinds->swap?> - swap languages</li>
                <li>Accesskey + <?=$this->config->keybinds->translate?> - translate</li>
                <li>Accesskey + <?=$this->config->keybinds->clearText?> - clear text</li>
            </ul>
        </details>
        <?php $this->checkJS() ?>
