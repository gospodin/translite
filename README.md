# TransLite

TransLite is a simple and lightweight frontend for Google Translate, DeepL, Yandex and DDG Translate written in PHP

[![status-badge](https://cicd.bloat.cat/api/badges/3/status.svg)](https://cicd.bloat.cat/repos/3)

## Installation

### Manually

You will need these php packages to run TransLite:

- php
- php-brotli
- php-curl
- php-dom
- php-fpm
- php-pecl-apcu
- php-session

You can run `composer install --dry-run` and if some php extensions are missing it will tell you what you should install.

If there's no needed extensions in your system package manager, you can either use [PECL](https://pecl.php.net/) or [composer](https://getcomposer.org/download/):

```bash
compose install --optimize-autoloader # for production environment
```

### Docker

Copy compose.example.yaml to compose.yaml, edit it to suit your needs and run `docker compose up -d`.

If an image is broken, you can use the tags linked to commits to revert to previous versions. This allows you to restore a working image while we address the issue.

## Instances
- https://tl.bloat.cat
- https://tl2.bloat.cat
- https://tl.clovius.club
- https://translite.lumaeris.com
- https://tl.dc09.ru
- https://tl.maid.zone
- https://translite.deep-swarm.xyz
- http://translite.dswarmsikhttkg7jgsoyfiqpj3ighupfrvuz5ri3lu5q2dlqyrpgk7ad.onion

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[The Unlicense](https://unlicense.org/)
