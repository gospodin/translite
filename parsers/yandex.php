<?php
require_once "parser.php";

class yandexParser extends Parser {
    public $parserName = "yandex";
    private $config;
    private $apiURL = "https://translate.yandex.net/api/v1/tr.json/translate";
    function __construct($config)
    {
        $this->config = $config;
    }
    private function _uuid($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);

        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    function getLanguages($mui) {
        $lang = [
            "af" => "Afrikaans",
            "sq" => "Albanian",
            "am" => "Amharic",
            "ar" => "Arabic",
            "hy" => "Armenian",
            "az" => "Azerbaijani",
            "ba" => "Bashkir",
            "eu" => "Basque",
            "be" => "Belarusian",
            "bn" => "Bengali",
            "bs" => "Bosnian",
            "bg" => "Bulgarian",
            "my" => "Burmese",
            "ca" => "Catalan",
            "ceb" => "Cebuano",
            "zh" => "Chinese (Simplified)",
            "cv" => "Chuvash",
            "hr" => "Croatian",
            "cs" => "Czech",
            "da" => "Danish",
            "nl" => "Dutch",
            "sjn" => "Elvish (Sindarin)",
            "emj" => "Emoji",
            "en" => "English",
            "eo" => "Esperanto",
            "et" => "Estonian",
            "fi" => "Finnish",
            "fr" => "French",
            "gl" => "Galician",
            "ka" => "Georgian",
            "de" => "German",
            "el" => "Greek",
            "gu" => "Gujarati",
            "ht" => "Haitian",
            "he" => "Hebrew",
            "mrj" => "Hill Mari",
            "hi" => "Hindi",
            "hu" => "Hungarian",
            "is" => "Icelandic",
            "id" => "Indonesian",
            "ga" => "Irish",
            "it" => "Italian",
            "ja" => "Japanese",
            "jv" => "Javanese",
            "kn" => "Kannada",
            "kazlat" => "Kazakh (Latin)",
            "kk" => "Kazakh",
            "km" => "Khmer",
            "ko" => "Korean",
            "ky" => "Kyrgyz",
            "lo" => "Lao",
            "la" => "Latin",
            "lv" => "Latvian",
            "lt" => "Lithuanian",
            "lb" => "Luxembourgish",
            "mk" => "Macedonian",
            "mg" => "Malagasy",
            "ms" => "Malay",
            "ml" => "Malayalam",
            "mt" => "Maltese",
            "mi" => "Maori",
            "mr" => "Marathi",
            "mhr" => "Mari",
            "mn" => "Mongolian",
            "ne" => "Nepali",
            "no" => "Norwegian",
            "pap" => "Papiamento",
            "fa" => "Persian",
            "pl" => "Polish",
            "pt-BR" => "Portuguese (Brazilian)",
            "pt" => "Portuguese",
            "pa" => "Punjabi",
            "ro" => "Romanian",
            "ru" => "Russian",
            "gd" => "Scottish Gaelic",
            "sr-Latn" => "Serbian (Latin)",
            "sr" => "Serbian",
            "si" => "Sinhalese",
            "sk" => "Slovak",
            "sl" => "Slovenian",
            "es" => "Spanish",
            "su" => "Sundanese",
            "sw" => "Swahili",
            "sv" => "Swedish",
            "tl" => "Tagalog",
            "tg" => "Tajik",
            "ta" => "Tamil",
            "tt" => "Tatar",
            "te" => "Telugu",
            "th" => "Thai",
            "tr" => "Turkish",
            "udm" => "Udmurt",
            "uk" => "Ukrainian",
            "ur" => "Urdu",
            "uzbcyr" => "Uzbek (Cyrillic)",
            "uz" => "Uzbek",
            "vi" => "Vietnamese",
            "cy" => "Welsh",
            "xh" => "Xhosa",
            "sah" => "Yakut",
            "yi" => "Yiddish",
            "zu" => "Zulu",
        ];
        if ($mui == "sl") {
            $lang['auto'] = "Autodetect";
        }
        return $lang;
    }
    function _translationGet($text, $sl, $tl) {
        $cachedTranslation = apcu_fetch($sl."_".$tl."_".$text."_yandex");
        if ($cachedTranslation !== false) {
            return $cachedTranslation;
        }
        $this->checkLanguages($sl, $tl);
        if ($sl == "auto") {
            $langCode = $tl;
        } else {
            $langCode = "$sl-$tl";
        }
        $query = [
            "lang" => $langCode,
            "text" => $text,
            "srv" => "android",
            "sid" => str_replace("-", "", $this->_uuid()) . "-0-0"
        ];
        $response = $this->requestPost($this->apiURL."?".http_build_query($query), "", ["Accept: application/json", "User-Agent: ru.yandex.translate/22.11.8.22364114 (samsung SM-A505GM; Android 12)"]);
        if (json_decode($response)->code != "200") {
            throw new Exception("Failed to translate");
        }
        apcu_store($sl."_".$tl."_".$text."_yandex", $response, $this->config->ttl);
        return $response;
    }
    function translate($text, $sl, $tl) {
        return json_decode($this->_translationGet($text, $sl, $tl))->text[0];
    }
    function getAdditionalData($text, $sl, $tl) {
        $cachedData = apcu_fetch($sl."_".$tl."_".$text."_yandex_data");
        if ($cachedData !== false) {
            return $cachedData;
        }
        $translationData = new TranslationData();
        $sourceLang = explode("-", json_decode($this->_translationGet($text, $sl, $tl))->lang)[0];
        $translationData->sourceLang = $sourceLang;
        if ($sl == "auto") {
            return $translationData;
        }
        $query = [
            "srv" => "tr-text",
            "ui" => "$tl",
            "src" => $text,
            "lang" => "$sl-$tl"
        ];
        $result = @json_decode($this->requestGet("https://dictionary.yandex.net/dicservice.json/queryCorpus?".http_build_query($query)))->result;
        if ($result == null) {
            return $translationData;
        }
        foreach ($result as $lemma) {
            $pos = $lemma->translation->pos->text ?? "Unknown";
            $examples = [];
            foreach ($lemma->examples as $example) {
                $examples[] = "$example->src --<br> $example->dst";
            }
            $translationData->addDictionaryWord($pos, $lemma->translation->text ?? "Unknown", "Examples", ...$examples);
        }
        apcu_store($sl."_".$tl."_".$text."_yandex_data", $translationData, $this->config->ttl);
        return $translationData;
    }
    function TranslateButton() {
        return "Translate";
    }
}
