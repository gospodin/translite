<?php
require_once "parser.php";
/*
class GoogleAdditionalData
{
    public $definitions = null;
    public $examples = null;
    public $synonyms = null;
    public $source_lang = null;
    public $slPronunciation = null;
    public $tlPronunciation = null;
    function __construct($json)
    {
        $this->slPronunciation = $json[0][0];
        $this->tlPronunciation = $json[1][0][0][1];
        if (isset($json[3])) {
            // [3][1][0] - definitions
            // foreach [0] - type [1] - values
            if (isset($json[3][1])) {
                $this->definitions = array();
                $this->_handleDefinitions($json[3][1][0]);
            }
            // [3][2][0] - usage examples
            // foreach [1]
            if (isset($json[3][2])) {
                $this->examples = array();
                foreach ($json[3][2][0] as $i) {
                    array_push($this->examples, $i[1]);
                }
            }
            // [3][5][0] - synonyms
            // foreach [0] - type [1] - values
            if (isset($json[3][5])) {
                $this->synonyms = array();
                $this->_handleSynonyms($json[3][5][0]);
            }
        }
        $this->source_lang = $json[2];
    }
    private function _handleDefinitions($definitions)
    {
        foreach ($definitions as $i) {
            // something is wrong with type, TODO
            //$type = $i[0] ?? "Unknown";
            $defs = [];
            foreach ($i[1] as $def_src) {
                $def = array();
                $def['definition'] = $def_src[0];
                $def['usage'] = $def_src[1] ?? "Unknown usage";
                $def['category'] = $def_src[4][0][0] ?? "Unknown";
                if (isset($def_src[5])) {
                    $def['synonyms'] = [];
                    foreach ($def_src[5] as $synonym_src) {
                        // $synonym_category = $synonym_src[1][0][0] ?? "Unknown";
                        $synonym_array = array();
                        foreach ($synonym_src[0] as $x) {
                            array_push($synonym_array, $x[0]);
                        }
                        //array_push($def['synonyms'], ["category" => $synonym_category, "synonyms" => $synonym_array]);
                        array_push($def['synonyms'], ["synonyms" => $synonym_array]);
                    }
                }
             //   array_push($defs, ["type" => $type, "definition" => $def]);
                array_push($defs, ["definition" => $def]);
            }
            array_push($this->definitions, $defs);
        }
    }
    private function _handleSynonyms($synonyms)
    {
        $this->synonyms = [];
        foreach ($synonyms as $i) {
            // something is wrong with type, TODO
            // $type = $i[0] ?? "Unknown";
            $syns = array();
            foreach ($i[1] as $synonym_src) {
                $syn = array();
                $syn['synonym'] = $synonym_src[0];
                $syn['translations'] = $synonym_src[2];
                $frequency = $synonym_src[3];
                if ($frequency == 3) {
                    $frequency = 1;
                } else if ($frequency == 1) {
                    $frequency = 3;
                }
                $syn['frequency'] = $frequency;
                array_push($syns, $syn);
            }
            //array_push($this->synonyms, ["type" => $type, "synonyms" => $syns]);
            array_push($this->synonyms, ["synonyms" => $syns]);
        }
    }
}
*/
class googleDParser extends Parser
{
    public $parserName = "googleD";
    private $config;
    function __construct($config)
    {
        $this->config = $config;
    }
    function getLanguages($mui)
    {
        $cachedLangs = apcu_fetch($mui."_langs_".$this->config->hl);
        if ($cachedLangs !== false) {
            return $cachedLangs;
        }
        $xpath = $this->getXpath($this->requestGet("https://translate.google.com/m?mui=$mui&hl=".$this->config->hl));
        $langs = array();
        foreach ($xpath->query("//div[contains(@class,'language-item')]/a") as $a) {
            $href = $xpath->query("@href", $a)->item(0)->nodeValue;
            parse_str(substr($href, 4), $query);
            $langs[$query[$mui]] = $a->nodeValue;
        }
        apcu_store($mui."_langs_".$this->config->hl, $langs, $this->config->ttl);
        return $langs;
    }
    function translate($text, $sl, $tl)
    {
        $cachedTranslation = apcu_fetch($sl."_".$tl."_".$text);
        if ($cachedTranslation !== false) {
            return $cachedTranslation;
        }
        $this->checkLanguages($sl, $tl);
        $query = urlencode($sl) . "&tl=" . urlencode($tl) . "&q=" . urlencode($text);
        $response = $this->requestGet("https://translate.google.com/m?sl=$query");
        $translation = $this->getXpath($response)->query("//div[@class='result-container']")->item(0)->nodeValue;
        apcu_store($sl."_".$tl."_".$text, $translation, $this->config->ttl);
        return $translation;
    }
    function tts($text, $tl)
    {
        $lang = 'en';
        if ($tl != 'auto' && $tl != '') {
            $lang = urlencode($tl);
        }
        $query = "tl=$lang&q=".urlencode($text)."&client=tw-ob";
        $request = curl_init("https://translate.google.com/translate_tts?$query");
        curl_setopt(
            $request,
            CURLOPT_WRITEFUNCTION,
            function ($request, $data) {
                echo $data;
                return strlen($data);
            }
        );
        header("Content-Type: audio/mpeg");
        curl_exec($request);
    }
    function getAdditionalData($text, $sl, $tl)
    {
        $cachedTranslationData = apcu_fetch($text."_".$sl."_".$tl."_data");
        if ($cachedTranslationData !== false) {
            return $cachedTranslationData;
        }
        $encodedText = trim(json_encode($text, JSON_UNESCAPED_UNICODE), '"');
        $encodedText = trim(json_encode($encodedText, JSON_UNESCAPED_UNICODE), '"');
        //phpcs:ignore
        $url = 'https://translate.google.com/_/TranslateWebserverUi/data/batchexecute?rpcids=MkEWBc';
        $parameter = 'f.req='.urlencode('[[["MkEWBc","[[\"'.$encodedText.'\",\"'.$sl.'\",\"'.$tl.'\",1],[]]",null,"generic"]]]&');
        $data = @json_decode(substr($this->requestPost($url, $parameter), 6))[0][2];
        // google ships json in json wtf
        if ($data == null) {
            return;
        }
        $json = json_decode($data);
        $translationData = new TranslationData();
        $translationData->sourceLang = $json[2];
        apcu_store($text."_".$sl."_".$tl."_data", $translationData, $this->config->ttl);
        return $translationData;
    }
    function TranslateButton()
    {
        return "Translate";
    }
}
