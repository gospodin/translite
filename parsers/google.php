<?php
require_once "parser.php";

class googleParser extends Parser
{
    public $parserName = "google";
    private $config;
    function __construct($config)
    {
        $this->config = $config;
    }
    function getLanguages($mui)
    {
        $cachedLangs = apcu_fetch($mui."_langs_".$this->config->hl);
        if ($cachedLangs !== false) {
            return $cachedLangs;
        }
        $xpath = $this->getXpath($this->requestGet("https://translate.google.com/m?mui=$mui&hl=".$this->config->hl));
        $langs = array();
        foreach ($xpath->query("//div[contains(@class,'language-item')]/a") as $a) {
            $href = $xpath->query("@href", $a)->item(0)->nodeValue;
            parse_str(substr($href, 4), $query);
            $langs[$query[$mui]] = $a->nodeValue;
        }
        apcu_store($mui."_langs_".$this->config->hl, $langs, $this->config->ttl);
        return $langs;
    }
    private function _getResponse($text, $sl, $tl) {
        $cachedResponse = apcu_fetch($sl."_".$tl."_".$text."_g2");
        if ($cachedResponse !== false) {
            return $cachedResponse;
        }
        $this->checkLanguages($sl, $tl);
        $response = $this->requestPost("https://translate.googleapis.com/translate_a/single", "client=gtx&dt=t&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&sl=${sl}&tl=${tl}&hl=${tl}&dj=1&source=bubble&q=".urlencode($text));
        apcu_store($sl."_".$tl."_".$text."_g2", $response, $this->config->ttl);
        return $response;
    }
    function translate($text, $sl, $tl)
    {
        $response = $this->_getResponse($text, $sl, $tl);
        $translation = "";
        foreach(@json_decode($response)->sentences as $sentence) {
            $translation = $translation . ($sentence->trans ?? "");
        }
        return $translation;
    }
    function getAdditionalData($text, $sl, $tl)
    {
        $translationData = new TranslationData();
        $response = @json_decode($this->_getResponse($text, $sl, $tl));
        $slPronunciation = "";
        $tlPronunciation = "";
        foreach($response->sentences as $sentence) {
            $slPronunciation = $slPronunciation . ($sentence->src_translit ?? "");
            $tlPronunciation = $tlPronunciation . ($sentence->translit ?? "");
        }
        $translationData->slPronunciation = $slPronunciation;
        $translationData->tlPronunciation = $tlPronunciation;
        $translationData->sourceLang = $response->ld_result->srclangs[0];
        foreach($response->dict ?? [] as $pos) {
            foreach($pos->entry as $word) {
                $translationData->addDictionaryWord($pos->pos, $word->word, "Reverse translation", ...$word->reverse_translation);
            }
        }
        foreach($response->synsets ?? [] as $pos) {
            foreach($pos->entry as $syngroup) {
                if (isset($syngroup->label_info->register)) {
                    if (is_array($syngroup->label_info->register)) {
                        $label = join(", ", $syngroup->label_info->register);
                    } else {
                        $label = $syngroup->label_info->register;
                    }
                }
                $translationData->addSynonym($pos->pos, $label ?? null, ...$syngroup->synonym);
                unset($label);
            }
        }
        foreach($response->definitions ?? [] as $pos) {
            foreach($pos->entry as $definition) {
                if (isset($definition->label_info->register)) {
                    if (is_array($definition->label_info->register)) {
                        $label = join(", ", $definition->label_info->register);
                    } else {
                        $label = $defintion->label_info->register;
                    }
                }
                $translationData->addDefinition($pos->pos, $label ?? null, $definition->gloss, $definition->example ?? null);
                unset($label);
            }
        }
        foreach($response->examples->example ?? [] as $example) {
            $translationData->addExample($example->text);
        }
        return $translationData;
    }
    function TranslateButton()
    {
        return "Translate";
    }
}
