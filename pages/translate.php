<?php
require_once "page.php";
class translate extends HTML
{
    protected $parser;
    protected $config;
    protected $translated;
    protected $additionalData;
    protected $request;
    function __construct(&$parser, $config)
    {
        $this->parser =& $parser;
        $this->config = $config;
        if (isset($_SESSION['error'])) {
            return;
        }
        if (isset($_SESSION['request'])) {
            $this->request = $_SESSION['request'];
            unset($_SESSION['request']);
        } else if ($this->config->usePOST && $_SERVER['REQUEST_METHOD'] == "POST") {
            $this->request = $_POST;
        } else {
            $this->request = $_GET;
        }
        if (isset($this->request['text'], $this->request['sl'], $this->request['tl'])) {
            try {
                $this->translated = $this->parser->translate($this->request['text'], $this->request['sl'], $this->request['tl']);
                $this->additionalData = $this->parser->getAdditionalData($this->request['text'], $this->request['sl'], $this->request['tl']);
            } catch (Exception $e) {
                $_SESSION['error'] = $e->getMessage();
            }
        }
        if (isset($this->request['tl'], $this->request['text'])) {
            if (isset($this->request['sl']) && $this->request['sl'] == "auto") {
                $query = $this->request;
                $query['sl'] = $this->additionalData->sourceLang;
                if (!$this->config->usePOST) {
                    header("Location: ./?".http_build_query($query));
                } else {
                    $_SESSION['request'] = $query;
                    header("Location: ./");
                }
                die();
            }
        }
    }
    function printEngines()
    {
        $engines = $this->config->parsers;
        $selected = $this->parser->parserName;
        echo "<input type=\"hidden\" name=\"engine\" value=\"$selected\">";
        foreach ($engines as $engineKey => $engineData) {
            if ($engineKey == "multi") {
                echo "<details><summary>Multiple engines</summary>";
                echo "<select multiple name=\"engines[]\">";
                foreach ($engines as $mEngineKey => $mEngineData) {
                    if ($mEngineKey == "multi") {
                        continue;
                    }
                    if (in_array($mEngineKey, $this->request["engines"] ?? [])) {
                        echo "<option selected value=\"$mEngineKey\">$mEngineData->name</option>";
                    } else {
                        echo "<option value=\"$mEngineKey\">$mEngineData->name</option>";
                    }
                }
                echo "</select><br>";
                echo "<button type=\"submit\" formaction=\"/api/toengine/$engineKey\">$engineData->name</button>";
                echo "</details>";
                break;
            }
            if ($selected == $engineKey) {
                echo "<button class=\"current\" type=\"submit\" formaction=\"/api/toengine/$engineKey\">$engineData->name</button>";
            } else {
                echo "<button type=\"submit\" formaction=\"/api/toengine/$engineKey\">$engineData->name</button>";
            }
        }
    }
    private function getLanguage($mui) {
        if (isset($this->request[$mui])) {
            $lang = $this->request[$mui];
        } else if ($mui == 'sl' && (!isset($this->request['sl']) || $this->request['sl'] == '')) {
            $lang = $this->config->parsers[$this->config->parser]->prefSl;
        } else {
            $lang = $this->config->parsers[$this->config->parser]->prefTl;
        }
        return $lang;
    }
    private function printLanguageDatalist($mui, $label, $key) {
        $lang = $this->getLanguage($mui);
        $defaultValue = "value=\"$lang\"";
        echo "<input id=\"{$mui}Select\" list=\"$mui\" aria-label=\"$label\" accesskey=\"$key\" name=\"$mui\" $defaultValue></input>";
        echo "<datalist id=\"$mui\">";
        foreach ($this->parser->getLanguages($mui) as $value => $text) {
            echo "<option value=\"$value\">$text</option>";
        }
        echo "</datalist>";
    }
    private function printLanguageInput($mui, $label, $key) {
        $lang = $this->getLanguage($mui);
        echo "<input id=\"{$mui}Select\"aria-label=\"$label\" accesskey=\"$key\" name=\"$mui\" value=\"$lang\"></input>";
    }
    private function printLanguageSelect($mui, $label, $key) {
        echo "<select id=\"{$mui}Select\" aria-label=\"$label\" accesskey=\"$key\" name=\"$mui\">";
        $lang = $this->getLanguage($mui);
        foreach ($this->parser->getLanguages($mui) as $value => $text) {
            if ($value == $lang) {
                echo "<option selected value=\"$value\">$text</option>";
            } else {
                echo "<option value=\"$value\">$text</option>";
            }
        }
        echo "</select>";
    }
    function printLanguages($mui)
    {
        $label = "Target language: ";
        if ($mui == "sl") {
            $label = "Source language: ";
        }
        $key = $this->config->keybinds->$mui;
        if ($this->config->prefLangSelect == "datalist") {
            $this->printLanguageDatalist($mui, $label, $key);
            return;
        } else if ($this->config->prefLangSelect == "input") {
            $this->printLanguageInput($mui, $label, $key);
            return;
        } else {
            $this->printLanguageSelect($mui, $label, $key);
        }
    }
    function printAdditionalData()
    {
        if (!isset($this->request['text'], $this->request['sl'], $this->request['tl'])) {
            return null;
        }
        $translation = $this->additionalData;
        if ($translation->getDictionary()) {
            echo "<h3 class=\"data_header\">Dictionary</h3>";
            echo "<div class=\"dictionary\">";
            foreach ($translation->getDictionary() as $pos) {
                echo "<ul>";
                echo "<span class=\"def_type\">$pos->pos</span>";
                foreach ($pos->words as $word) {
                    echo "<li class=\"syn\">$word->word";
                    if ($word->helpers) {
                        echo "<details>";
                        echo "<summary>{$word->helpers->type}</summary>";
                        echo "<ul class=\"syn\">";
                        foreach ($word->helpers->list as $helper) {
                            echo "<li>$helper</li>";
                        }
                        echo "</ul>";
                        echo "</details>";
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
            echo "</div>";
        }
        if ($translation->getSynonyms()) {
            echo "<h3 class=\"data_header\">Synonyms</h3>";
            echo "<div class=\"synonyms\">";
            foreach ($translation->getSynonyms() as $pos) {
                echo "<details>";
                echo "<summary><span class=\"def_type\">$pos->pos</span></summary>";
                echo "<ul>";
                foreach ($pos->synonyms as $synonym) {
                    echo "<li><details open>";
                    if (isset($synonym->label)) {
                        echo "<summary>$synonym->label</summary>";
                    } else {
                        echo "<summary>no label</summary>";
                    }
                    echo "<ul class=\"syn\">";
                    foreach($synonym->synonym as $synonym_word) {
                        echo "<li>$synonym_word</li>";
                    }
                    echo "</ul>";
                    echo "</details></li>";
                }
                echo "</ul>";
                echo "</details>";
            }
            echo "</div>";
        }
        if ($translation->getDefinitions()) {
            echo "<h3 class=\"data_header\">Definitions</h3>";
            echo "<div class=\"synonyms\">";
            foreach ($translation->getDefinitions() as $pos) {
                echo "<ul>";
                echo "<span class=\"def_type\">$pos->pos</span>";
                foreach($pos->definitions as $definition) {
                    echo "<li>";
                    echo "$definition->definition<br>";
                    if ($definition->example) {
                        echo "<i>$definition->example</i><br>";
                    }
                    if ($definition->label) {
                        echo "<b>$definition->label</b>";
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
            echo "</div>";
        }
        if ($translation->getExamples()) {
            echo "<h3 class=\"data_header\">Examples</h3>";
            echo "<ul>";
            foreach($translation->getExamples() as $example) {
                echo "<li>$example</li>";
            }
            echo "</ul>";
        }
    }
    function slTextarea()
    {
        echo isset($this->request['text']) ? htmlspecialchars($this->request['text']) : "";
    }
    function TTSLink($mui, $text)
    {
        if (!method_exists($this->parser, 'tts')) {
            return null;
        }
        $key = $this->config->keybinds->{$mui."TTS"};
        if (isset($this->request[$mui], $this->request['text'], $this->request['engine']) && $this->request['text'] != "") {
            $url = '/api/tts?engine='.$this->request['engine'].'&tl='.urlencode($this->request[$mui]).'&text='.urlencode($text);
            echo "<audio accesskey=\"$key\" controls><source type=\"audio/mpeg\" src=\"$url\"></audio>";
        }
    }
    function slTTSLink()
    {
        if (isset($_SESSION['error'])) {
            return null;
        }
        if (isset($this->request['text'])) {
            $this->TTSLink('sl', $this->request['text']);
        }
    }
    function tlTextarea()
    {
        echo $this->translated;
    }
    function tlTTSLink()
    {
        if (isset($_SESSION['error'])) {
            return null;
        }
        $this->TTSLink('tl', $this->translated);
    }
    function translateButton()
    {
        echo $this->parser->TranslateButton();
    }
    function showError()
    {
        if (isset($_SESSION['error'])) {
            echo '<div style="color:red" class="center">'.$_SESSION['error'].'</div>';
            unset($_SESSION['error']);
        }
    }
    function checkJS()
    {
        if ($this->config->jsEnabled) {
            echo "<script src=\"/static/fluoride.js\"></script>";
        }
    }
    function getMethod()
    {
        if ($this->config->usePOST) {
            return "POST";
        }
        return "GET";
    }
    function printMaxLength()
    {
        if ($this->config->usePOST) {
            return;
        }
        echo "maxlength=\"{$this->config->maxLinkLength}\"";
    }
    function printPronunciation($mui) {
        if (isset($this->request['sl'], $this->request['tl'], $this->request['text'])) {
            if ($this->additionalData->slPronunciation && $mui == "sl") {
                echo "<details class=\"pronunciation\"><summary>Pronunciation</summary>{$this->additionalData->slPronunciation}</details>";
            }
            if ($this->additionalData->tlPronunciation && $mui == "tl") {
                echo "<details class=\"pronunciation\"><summary>Pronunciation</summary>{$this->additionalData->tlPronunciation}</details>";
            }

        }
    }
}
