<?php

class TranslationData implements JsonSerializable
{
    private $definitions = null;
    /*
        [
            {
                pos: "part of speech",
                definitions: [
                    {
                        definition: "def",
                        example: "example",
                        label: "label"
                    }
                ]
            }
        ]
    */
    private $examples = null;
    /*
        [
            "example1",
            "example2"
        ]
    */
    private $synonyms = null;
    /*
        [
            {
                pos: "part of speech",
                synonyms: [
                    {
                        label: "label",
                        synonym: [
                        "string",
                        "string"
                        ]
                    }
                ]
            }
        ]
    */
    private $dictionary = null;
    /*
        [
            {
                pos: "part of speech",
                words: [
                    {
                        word: "word",
                        helpers: {
                                type: "examples"||"reverse translations",
                                list: [
                                "string",
                                "string"
                                ]
                        }
                    }
                ]
            }
        ]
    */
    public ?string $slPronunciation = null;
    public ?string $tlPronunciation = null;
    public ?string $sourceLang = null;
    public ?string $translation = null;
    function addDictionaryWord(string $partOfSpeech, string $word, string $helperType, string ...$helpers) {
        $wordObject = (object)["word" => $word, "helpers" => (object)["type" => $helperType, "list" => []]];
        if ($helpers == []) {
            $wordObject->helpers = null;
        } else {
            foreach($helpers as $helper) {
                $wordObject->helpers->list[] = $helper;
            }
        }
        if ($this->dictionary == null) {
           $this->dictionary = [];
        }
        $found = -1;
        foreach($this->dictionary as $k => $v) {
            if ($v->pos == $partOfSpeech) {
                $found = $k;
            }
        }
        if ($found == -1) {
            $this->dictionary[] = (object)["pos" => $partOfSpeech, "words" => []];
            $found = array_key_last($this->dictionary);
        }
        $this->dictionary[$found]->words[] = $wordObject;
    }
    function getDictionary() {
        return $this->dictionary;
    }
    function addSynonym(string $partOfSpeech, ?string $label, string ...$synonym) {
        $synonymObject = (object)["label" => $label, "synonym" => $synonym];
        if ($this->synonyms == null) {
           $this->synonyms = [];
        }
        $found = -1;
        foreach($this->synonyms as $k => $v) {
            if ($v->pos == $partOfSpeech) {
                $found = $k;
            }
        }
        if ($found == -1) {
            $this->synonyms[] = (object)["pos" => $partOfSpeech, "synonyms" => []];
            $found = array_key_last($this->synonyms);
        }
        $this->synonyms[$found]->synonyms[] = $synonymObject;
    }
    function getSynonyms() {
        return $this->synonyms;
    }
    function addDefinition(string $partOfSpeech, ?string $label, string $definition, ?string $example)
    {
        $definitionObject = (object)["definition" => $definition, "example" => $example, "label" => $label];
        if ($this->definitions == null) {
           $this->definitions = [];
        }
        $found = -1;
        foreach($this->definitions as $k => $v) {
            if ($v->pos == $partOfSpeech) {
                $found = $k;
            }
        }
        if ($found == -1) {
            $this->definitions[] = (object)["pos" => $partOfSpeech, "definitions" => []];
            $found = array_key_last($this->definitions);
        }
        $this->definitions[$found]->definitions[] = $definitionObject;
    }
    function getDefinitions() {
        return $this->definitions;
    }
    function addExample(string $example) {
        $this->examples[] = $example;
    }
    function getExamples() {
        return $this->examples;
    }
    
    function jsonSerialize() {
        return [
            "definitions" => $this->definitions,
            "examples" => $this->examples,
            "synonyms" => $this->synonyms,
            "dictionary" => $this->dictionary,
            "slPronunciation" => $this->slPronunciation,
            "tlPronunciation" => $this->tlPronunciation,
            "sourceLang" => $this->sourceLang
        ];
    }
}

abstract class Parser
{
    function requestGet($url)
    {
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($request);
        curl_close($request);
        return $result;
    }
    function requestPost($url, $post_parameter, $headers = null)
    {
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_parameter);
        if (!is_null($headers)) {
            curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        }
        $result = curl_exec($request);
        curl_close($request);
        return $result;
    }
    function getXpath($response)
    {
        $htmlDom = new \DOMDocument();
        @$htmlDom->loadHTML("<?xml encoding=\"UTF-8\">" . $response);
        $xpath = new \DOMXPath($htmlDom);
        return $xpath;
    }
    abstract function getLanguages($mui);
    function checkLanguages($sl, $tl) {
        if (!array_key_exists($sl, $this->getLanguages('sl')) || !array_key_exists($tl, $this->getLanguages('tl'))) {
            throw new Exception("Invalid languages");
        }
    }
}
